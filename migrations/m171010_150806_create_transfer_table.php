<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transfer`.
 */
class m171010_150806_create_transfer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%transfer}}', [
            'id' => $this->primaryKey(),
            'user_from' => $this->integer()->notNull(),
            'user_to' => $this->integer()->notNull(),
            'amount' => $this->decimal(10,2)->unsigned()->notNull(),
            'created_at' => $this->timestamp(),
        ]);
        $this->addForeignKey('user_from_fk', '{{%transfer}}', 'user_from', '{{%user}}', 'id');
        $this->addForeignKey('user_to_fk', '{{%transfer}}', 'user_to', '{{%user}}', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%transfer}}');
    }
}
