<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171010_135059_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique()->notNull(),
            'balance' => $this->decimal(10,2)->defaultValue(0)->notNull(),
            'version' => $this->bigInteger()->defaultValue(0)->notNull()->comment('version field for optimistic locks'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
