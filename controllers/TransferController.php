<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Transfer;
use app\models\TransferSearch;
use yii\filters\AccessControl;

class TransferController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['history', 'send'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * List of user's transaction history.
     * @return string
     */
    public function actionHistory()
    {
        $searchModel = new TransferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('history', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Send money to another user account
     * @return mixed
     */
    public function actionSend()
    {
        $model = new Transfer();

        //try to send money
        if ($model->load(\Yii::$app->request->post()) && $model->send()) {
            Yii::$app->session->setFlash('success', 'Money successfully transferred');
            return $this->redirect(['history']);
        } elseif (Yii::$app->request->isPost) {
            Yii::$app->session->setFlash('error', 'Money transfer failed');
        }

        return $this->render('send', [
            'model' => $model,
        ]);
    }


}
