<?php
namespace app\tests\models;
use app\models\User;
use app\tests\fixtures\UserFixture;
use yii\codeception\DbTestCase;

class UserTest extends DbTestCase
{
    public function fixtures()
    {
        return [
            'user' => UserFixture::className(),
        ];
    }

    public function testFindUserById()
    {
        $user = User::findIdentity(1);
        $this->assertInstanceOf(User::className(), $user);
        $this->assertEquals('user1', $user->username);

        $this->assertNull(User::findIdentity(999));
    }

    public function testCreatingNewUser()
    {
        $user = User::getUser('user3');
        $this->assertInstanceOf(User::className(), $user);
        $this->assertEquals('user3', $user->username);
        $this->assertNotNull($user->id);
        $this->assertEquals(0, $user->balance);
    }




}
