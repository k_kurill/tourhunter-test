<?php
namespace app\tests\models;
use app\models\Transfer;
use app\models\User;
use app\tests\fixtures\UserFixture;
use yii\codeception\DbTestCase;
use Yii;

class TransferTest extends DbTestCase
{
    public function fixtures()
    {
        return [
            'user' => UserFixture::className(),
        ];
    }


    public function testTransferToHimself()
    {
        Transfer::deleteAll();
        Yii::$app->user->login(User::findIdentity(1));
        $this->assertFalse(Yii::$app->user->isGuest);
        $transfer = new Transfer([
            'username' => 'user1',
            'amount' => 10,
        ]);

        $user1 = User::findIdentity(1);
        $this->assertFalse($transfer->send());
        $this->assertEquals(10, $user1->balance);
        $this->assertEquals(0, Transfer::find()->count());
    }

    public function testTransferToUser()
    {
        $amount = 10;
        Yii::$app->user->login(User::findIdentity(1));
        $this->assertFalse(Yii::$app->user->isGuest);
        $transfer = new Transfer([
            'username' => 'user2',
            'amount' => $amount,
        ]);

        $user1 = User::findOne(1);
        $oldBalanceUser1 = $user1->balance;

        $user2 = User::findOne(2);
        $oldBalanceUser2 = $user2->balance;

        $this->assertTrue($transfer->send());
        $user1->refresh();
        $user2->refresh();

        $this->assertEquals($oldBalanceUser1 - $amount, $user1->balance);
        $this->assertEquals($oldBalanceUser2 + $amount, $user2->balance);
        $this->assertEquals(1, Transfer::find()->count());
        Transfer::deleteAll();
    }

    public function testTransferLessThanZero()
    {
        $amount = -10;
        Yii::$app->user->login(User::findIdentity(1));
        $this->assertFalse(Yii::$app->user->isGuest);
        $transfer = new Transfer([
            'username' => 'user2',
            'amount' => $amount,
        ]);

        $user1 = User::findOne(1);
        $oldBalanceUser1 = $user1->balance;

        $user2 = User::findOne(2);
        $oldBalanceUser2 = $user2->balance;

        $this->assertFalse($transfer->send());
        $user1->refresh();
        $user2->refresh();

        $this->assertEquals($oldBalanceUser1, $user1->balance);
        $this->assertEquals($oldBalanceUser2, $user2->balance);
        $this->assertEquals(0, Transfer::find()->count());
        Transfer::deleteAll();
    }

    public function testTransferZero()
    {
        $amount = 0;
        Yii::$app->user->login(User::findIdentity(1));
        $this->assertFalse(Yii::$app->user->isGuest);
        $transfer = new Transfer([
            'username' => 'user2',
            'amount' => $amount,
        ]);

        $user1 = User::findOne(1);
        $oldBalanceUser1 = $user1->balance;

        $user2 = User::findOne(2);
        $oldBalanceUser2 = $user2->balance;

        $this->assertFalse($transfer->send());
        $user1->refresh();
        $user2->refresh();

        $this->assertEquals($oldBalanceUser1, $user1->balance);
        $this->assertEquals($oldBalanceUser2, $user2->balance);
        $this->assertEquals(0, Transfer::find()->count());
        Transfer::deleteAll();
    }

}
