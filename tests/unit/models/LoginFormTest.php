<?php

namespace app\tests\models;

use app\models\LoginForm;
use app\models\User;
use app\tests\fixtures\UserFixture;
use yii\codeception\DbTestCase;

class LoginFormTest extends DbTestCase
{
    private $model;

    public function fixtures()
    {
        return [
            'user' => UserFixture::className(),
        ];
    }


    public function testLoginNewUser()
    {
        $this->model = new LoginForm([
            'username' => 'test',
        ]);
        $this->assertTrue($this->model->login());
        $this->assertNotTrue(\Yii::$app->user->isGuest);
        $this->assertEmpty($this->model->errors);
        //is new user exist
        $user = User::find()->andWhere(['username' => 'test'])->one();
        $this->assertNotNull($user);
        $user->delete();
    }

    public function testLoginExistedUser()
    {
        $this->model = new LoginForm([
            'username' => 'user1',
        ]);
        $this->assertTrue($this->model->login());
        $this->assertNotTrue(\Yii::$app->user->isGuest);
        $this->assertEquals(2, User::find()->count());
    }

}
