<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 11.10.17
 * Time: 14:41
 */
return [
    [
        'id' => 1,
        'username' => 'user1',
        'balance' => 10,
    ],
    [
        'id' => 2,
        'username' => 'user2',
        'balance' => 50,
    ],
];
