<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 11.10.17
 * Time: 14:36
 */

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = 'app\models\User';

}