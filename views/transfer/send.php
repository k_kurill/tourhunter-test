<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Transfer */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Send money';
?>

<div class="transfer-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
    ]); ?>


    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'amount')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' => 'currency',
        ],
        'aliases' => [
            'currency' => [
                'groupSeparator' => '',
                'autoGroup' => false,
                'allowMinus' => false,
                'integerDigits' =>10,
                'prefix'=>'',
            ],
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
