<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TransferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transfer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Transfer', ['send'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => null,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => Html::tag('span', 'From',['class' => "text-success"]) . ' / ' . Html::tag('span', 'To',['class' => "text-danger"]),
                'encodeLabel' => false,
                'format' => 'html',
                'value' => function($model) {
                    if($model->user_to == Yii::$app->user->id) {
                        return Html::tag('span', $model->userFrom->username,['class' => "text-success"]);
                    }
                    return Html::tag('span', $model->userTo->username,['class' => "text-danger"]);
                }
            ],
            'amount:decimal',
            'created_at:datetime'
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>