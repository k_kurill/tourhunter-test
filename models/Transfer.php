<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "abc_transfer".
 *
 * @property integer $id
 * @property integer $user_from
 * @property integer $user_to
 * @property string $amount
 * @property string $created_at
 *
 * @property User $userFrom
 * @property User $userTo
 */
class Transfer extends \yii\db\ActiveRecord
{


    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transfer}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_from', 'user_to'], 'integer'],
            [['amount', 'user_from', 'user_to'], 'required'],
            [['amount'], 'double', 'numberPattern' => '/^[0-9]*[.]?[0-9]{0,2}$/', 'message' => 'Amount must be in decimal format, with two numbers after the dot'],
            [['amount'], 'double', 'min' => '0.01'],
            [['created_at'], 'safe'],
            [['username'], 'string', 'max' => 255],
            [['username'], 'trim'],
            [['!user_from'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_from' => 'id']],
            [['!user_to'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_to' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_from' => 'User From',
            'userTo.username' => 'User To',
            'amount' => 'Amount',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_to']);
    }

    /**
     * Transfer money from the account of the current user to [[username]]
     * @return bool
     */
    public function send()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            //Find user models
            $userFrom = User::findOne(Yii::$app->user->id);
            $userTo = User::getUser($this->username);
            if (!$userFrom || !$userTo) {
                throw new Exception('User not found');
            }
            //Changing the balance
            $userFrom->balance -= $this->amount;
            $this->user_from = $userFrom->id;

            $userTo->balance += $this->amount;
            $this->user_to = $userTo->id;

            //Saving all models
            if (!$userFrom->save(true, ['balance'])
                || !$userTo->save(true, ['balance'])
                || !$this->save()) {

                throw new Exception('Users balance was not saved');
            }
            $transaction->commit();
            return true;
        } catch (\Exception $exception) {
            $transaction->rollBack();
        }
        return false;
    }
}
